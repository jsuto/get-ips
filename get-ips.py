#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import netinfo


def main():
    parser = argparse.ArgumentParser(description="Getting IP-addresses")
    parser.add_argument("--with-prefix", help="With prefix", action="store_true")
    parser.add_argument("--overlapping", help="Overlapping", action="store_true")
    parser.add_argument("--broadcast", help="Broadcast address", action="store_true")

    args = parser.parse_args()

    ips = []

    interfaces = netinfo.get_if_list()
    for interface in interfaces:
        ips.append(netinfo.get_if_info(interface))

    if args.overlapping:
        ips = netinfo.get_overlapping_addresses(ips)

    for ip in ips:
        if args.with_prefix or args.overlapping:
            print '{:s}/{:d}'.format(ip[0], ip[1])
        elif args.broadcast:
            print '{:s}/{:d} {:s}'.format(ip[0], ip[1], ip[3])
        else:
            print ip[0]


if __name__ == "__main__":
    main()
