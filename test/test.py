import netinfo
import unittest


class TestIPAddress(unittest.TestCase):

    def test_no_overlap(self):
        data = [
            ('1.2.3.4', 32, '00000001000000100000001100000100'),
            ('127.0.0.1', 8, '11111110'),
            ('192.168.0.1', 24, '110000001010100000000000')
        ]

        expected_results = []

        results = netinfo.get_overlapping_addresses(data)
        self.assertEqual(expected_results, results)

    def test_no_overlap_some_addresses_on_the_same_network(self):
        data = [
            ('1.2.3.4', 32, '00000001000000100000001100000100'),
            ('127.0.0.1', 8, '11111110'),
            ('192.168.0.1', 24, '110000001010100000000000'),
            ('192.168.0.2', 24, '110000001010100000000000'),
            ('192.168.0.3', 24, '110000001010100000000000'),
        ]

        expected_results = []

        results = netinfo.get_overlapping_addresses(data)
        self.assertEqual(expected_results, results)

    def test_two_overlapping_addresses(self):
        data = [
            ('1.2.3.4', 32, '00000001000000100000001100000100'),
            ('1.2.3.5', 31, '0000000100000010000000110000010'),
            ('127.0.0.1', 8, '11111110'),
            ('192.168.0.1', 16, '1100000010101000'),
            ('192.168.2.1', 24, '110000001010100000000010'),
        ]

        expected_results = [
            ('1.2.3.4', 32, '00000001000000100000001100000100'),
            ('1.2.3.5', 31, '0000000100000010000000110000010'),
            ('192.168.0.1', 16, '1100000010101000'),
            ('192.168.2.1', 24, '110000001010100000000010'),
        ]

        results = sorted(netinfo.get_overlapping_addresses(data))
        self.assertEqual(expected_results, results)

    def test_get_binary_address_from_4_bytes(self):
        data = [
            (1, 2, 3, 4),
            (127, 0, 0, 1),
            (172, 16, 0, 1),
            (192, 168, 1, 1)
        ]

        expected_results = [
            '00000001000000100000001100000100',
            '01111111000000000000000000000001',
            '10101100000100000000000000000001',
            '11000000101010000000000100000001'
        ]

        results = []

        for addr_bytes in data:
            s = ''.join([chr(x) for x in addr_bytes])
            results.append(netinfo.get_binary_address_from_4_bytes(s))

        self.assertEqual(expected_results, results)

    def test_get_network_size(self):
        data = [
            '11111111111111110000000000000000',
            '11111110111111110000000000000000',
            '11111111111111111111000000000000',
            '11111111111111111111111111111111',
        ]

        expected_results = [
            16,
            -1,
            20,
            32
        ]

        results = []

        for netmask in data:
            results.append(netinfo.get_network_size(netmask))

        self.assertEqual(expected_results, results)


if __name__ == "__main__":
    unittest.main()
