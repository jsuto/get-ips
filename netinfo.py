# -*- coding: utf-8 -*-

import array
import fcntl
import ipaddress
import socket
import struct


def get_if_list():
    ifaces = []

    try:
        fd = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        max_possible = 8
        struct_size = 40

        while True:
            bytes = max_possible * struct_size
            names = array.array('B')
            for i in range(0, bytes):
                names.append(0)

            input_buffer = struct.pack('iL', bytes, names.buffer_info()[0])

            output_buffer = fcntl.ioctl(fd.fileno(), 0x8912, input_buffer)
            output_size = struct.unpack('iL', output_buffer)[0]

            # If output_size == bytes, there may be more interfaces.
            if output_size == bytes:
                max_possible *= 2
            else:
                break

        namestr = names.tostring()
        for i in range(0, output_size, struct_size):
            iface_name = namestr[i:i+16].split('\0', 1)[0]
            ifaces.append(iface_name)
        fd.close()
    except IOError:
        print "Unable to call ioctl with SIOCGIFCONF"

    return ifaces


def get_binary_address_from_4_bytes(byte_buffer):
    return '{:0>32s}'.format(bin(int(ipaddress.IPv4Address(byte_buffer)))[2:])


def get_packed_ipaddr_on_interface(ifname, action):
    input_buffer = struct.pack('256s', ifname)

    try:
        fd = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        output_buffer = fcntl.ioctl(fd.fileno(), action, input_buffer)
        fd.close()
    except IOError:
        print "Unable to call ioctl with 0x{:x} on {:s}".format(action, ifname)

    return output_buffer[20:24]


def get_network_size(s):
    # What are the chances the we get an invalid network
    # address mask, eg. 1111011110000...00 from the kernel?

    s = s.rstrip("0")
    if len(s) != len([x for x in s if x == '1']):
        return -1

    return len(s)


def get_if_info(ifname):
    ipaddr_bytes = get_packed_ipaddr_on_interface(ifname, 0x8915)
    ipaddr = ipaddress.IPv4Address(ipaddr_bytes)

    netmask_bytes = get_packed_ipaddr_on_interface(ifname, 0x891b)
    netmask_binary = get_binary_address_from_4_bytes(netmask_bytes)

    network_size = get_network_size(netmask_binary)
    network_bin_addr = get_binary_address_from_4_bytes(ipaddr_bytes)[:network_size]

    broadcast_addr_bytes = get_packed_ipaddr_on_interface(ifname, 0x8919)
    broadcast_addr = ipaddress.IPv4Address(broadcast_addr_bytes)

    return (ipaddr, network_size, network_bin_addr, broadcast_addr)


def get_overlapping_addresses(ips):
    overlapping = {}

    while ips:
        (ip1, size1, net1) = ips.pop()

        for (ip2, size2, net2) in ips:
            # Can't be an overlap if the sizes of the networks are the same
            if size1 != size2:
                if size1 >= size2:
                    size = size2
                else:
                    size = size1

                if net1[:size] == net2[:size]:
                    overlapping[ip1] = (ip1, size1, net1)
                    overlapping[ip2] = (ip2, size2, net2)

    return overlapping.values()
